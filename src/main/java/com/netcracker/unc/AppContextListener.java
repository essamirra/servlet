package com.netcracker.unc;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Created by Maria Pronina.
 */
@WebListener
public class AppContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext ctx = servletContextEvent.getServletContext();
        ctx.setAttribute("db", new ImageDatabase());
        ctx.setAttribute("renderer", new TableRenderer());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
