package com.netcracker.unc;

import java.util.List;

/**
 * Created by maria on 08.05.2017.
 */
public interface IImageDatabase {
    ImageTableData putImageToDatabase(RawImageData d);
    ImageTableData updateLikes(int imageId, int newLikesCount);
    List<ImageTableData> getImages(List<Integer> id, String category, Integer likes);
    ImageTableData getImage(int imageId);
}
