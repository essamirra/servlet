package com.netcracker.unc;

import java.util.List;

/**
 * Created by Maria Pronina.
 */
public interface IImageTableRenderer {
    String buildTable(List<ImageTableData> images);
    String buildTable(ImageTableData images);
}
