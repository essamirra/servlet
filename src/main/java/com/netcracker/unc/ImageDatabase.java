package com.netcracker.unc;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Maria Pronina.
 */
public class ImageDatabase implements IImageDatabase {
    private ConcurrentHashMap<Integer, ImageTableData> map = new ConcurrentHashMap<>();
    private AtomicInteger idGenerator = new AtomicInteger();


    @Override
    public ImageTableData putImageToDatabase(RawImageData d) {
        ImageTableData newRow = new ImageTableData(d.getName(), idGenerator.getAndIncrement(),
                new Date(d.getDate() * 1000), 0, d.getCategory(), d.getUrl());
        map.put(newRow.getId(), newRow);
        return newRow;
    }

    @Override
    public ImageTableData updateLikes(int imageId, int newLikesCount) {
        map.get(imageId).setLikes(newLikesCount);
        return map.get(imageId);
    }

    @Override
    public List<ImageTableData> getImages(List<Integer> id, String category, Integer likes) {
        List<ImageTableData> result = new LinkedList<>();
        if (id.isEmpty()) {
            for (Map.Entry<Integer, ImageTableData> o :
                    map.entrySet()) {
                ImageTableData data = o.getValue();
                filter(category, likes, result, data);
            }
        } else {
            for (Map.Entry<Integer, ImageTableData> o :
                    map.entrySet()) {
                ImageTableData data = o.getValue();
                if (id.contains(data.getId())) {
                    filter(category, likes, result, data);
                }
            }
        }
        return result;
    }

    private void filter(String category, Integer likes, List<ImageTableData> result, ImageTableData data) {
        if (category != null) {
            if (data.getCategory().equals(category)) {
                if(likes != null) {
                    if (data.getLikes() == likes) {
                        result.add(data);
                    }
                }
                else result.add(data);
            }
        } else if (likes != null) {
            if (data.getLikes() == likes) {
                result.add(data);
            }
        } else result.add(data);

    }

    @Override
    public ImageTableData getImage(int imageId) {
        return map.get(imageId);
    }
}
