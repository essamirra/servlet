package com.netcracker.unc;

import java.util.Date;

/**
 * Created by Maria Pronina.
 */
public class ImageTableData {
    private String name;
    private int id;
    private Date date;
    private int likes;
    private String category;
    private String url;

    public ImageTableData(String name, int id, Date date, int likes, String category, String url) {
        this.name = name;
        this.id = id;
        this.date = date;
        this.likes = likes;
        this.category = category;
        this.url = url;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
