package com.netcracker.unc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by Maria Pronina.
 */


public class ImageTableServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(ServletExample.class);
    private  IImageDatabase db;
    private  IImageTableRenderer tableRenderer;

    @Override
    public void init() throws ServletException {
        db = (IImageDatabase)getServletContext().getAttribute("db");
        tableRenderer = (IImageTableRenderer)getServletContext().getAttribute("renderer");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestBody = getRequestBody(req);
        ObjectMapper mapper = new ObjectMapper();
        RawImageData imageData = mapper.readValue(requestBody,RawImageData.class);
        ImageTableData row = db.putImageToDatabase(imageData);
        try (PrintWriter out = resp.getWriter()) {
            out.write(tableRenderer.buildTable(row));
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
            throw e;
        }


    }

    private String getRequestBody(HttpServletRequest req) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = req.getReader();
        String str;
        while( (str = br.readLine()) != null ){
            sb.append(str);
        }
        str = sb.toString();
        return str;
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestBody = getRequestBody(req);
        int imageId = 0;
        //debugOutput(resp, req.getParameter("id"));
        imageId = Integer.parseInt(req.getParameter("id"));



        int newLikesCount = 0;
        ObjectNode node = new ObjectMapper().readValue(requestBody, ObjectNode.class);
        if (node.has("likes")) {
            newLikesCount = Integer.parseInt(node.get("likes").asText());
        }
       // debugOutput(resp, String.valueOf(newLikesCount));
        db.updateLikes(imageId, newLikesCount);
        showTable(resp, imageId);

    }

    private void debugOutput(HttpServletResponse resp, String s) throws IOException {
        String DYNAMIC_PAGE_CONTENT = "" +
                "<HTML>" +
                "   <BODY>" +
                "%1$s" +
                "</BODY>" +
                "</HTML>";
        try (PrintWriter out = resp.getWriter()) {
            out.write(String.format(DYNAMIC_PAGE_CONTENT, s));
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
            throw e;
        }
    }

    private void showTable(HttpServletResponse resp, int imageId) throws IOException {
        try (PrintWriter out = resp.getWriter()) {
            out.write(tableRenderer.buildTable(db.getImage(imageId)));
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
            throw e;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String imagesIds = req.getParameter("id");
        String category = req.getParameter("category");
        String likes = req.getParameter("likes");
        List<Integer> id = new LinkedList<>();
        if(imagesIds != null)
            for (String idStr:imagesIds.split(",")
                 ) {
                id.add(Integer.parseInt(idStr));
            }
        Integer likesInt;
        if(likes == null)
            likesInt = null;
        else
            likesInt = Integer.parseInt(likes);
       List<ImageTableData> imageDataList =  db.getImages(id, category, likesInt);

       if(imageDataList.isEmpty())
           debugOutput(resp,"No results");
       else {
           try (PrintWriter out = resp.getWriter()) {
               out.write(tableRenderer.buildTable(imageDataList));
           } catch (IOException e) {
               logger.error(e.getLocalizedMessage());
               throw e;
           }
       }

    }
}
