package com.netcracker.unc;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class MyServlet1 extends HttpServlet {
    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req != null) {
            String queryString = req.getQueryString();
            if (queryString != null) {
                String[] result = queryString.split("=");
                if ((result.length != 2) || (!result[0].equals("category"))) {
                    throw new RuntimeException("Bad Request");
                }

                String categoryValue = result[1];
                BufferedReader bufReader = req.getReader();
                String body = bufReader.readLine();

                PrintWriter writer = resp.getWriter();
                writer.print("aa");
            }
        }
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }
}
