package com.netcracker.unc;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ServletExample extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(ServletExample.class);
    private static final String DYNAMIC_PAGE_CONTENT = "" +
            "<HTML>" +
            "   <BODY>Hello, %1$s!</BODY>" +
            "</HTML>";


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (PrintWriter out = response.getWriter()) {
            String user = request.getHeader("Authorization");

            if (user == null) {
                user = "Anonymous";
            }

            out.print(String.format(DYNAMIC_PAGE_CONTENT, user));
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
            throw e;
        }


    }
}
