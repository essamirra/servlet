package com.netcracker.unc;

import java.util.List;

/**
 * Created by Maria Pronina.
 */
public class TableRenderer implements IImageTableRenderer {
    private static final String DYNAMIC_PAGE_CONTENT = "" +
            "<HTML>" +
            "   <BODY>" +
            "<table border=\"1\">" +
            "<tr><th>id</th><th>name</th><th>category</th><th>date</th><th>url</th><th>likes</th></tr>" +
            "%1$s</table>" +
            "</BODY>" +
            "</HTML>";
    private static final String tableRow =
            "<tr><td>%1$s</td><td>%2$s</td><td>%3$s</td><td>%4$s</td><td>%5$s</td><td>%6$s</td></tr>";
    private static final String link = "<a href=\"\">%1$s</a>";


    @Override
    public String buildTable(List<ImageTableData> image) {
        StringBuilder builder = new StringBuilder("");
        for (ImageTableData images:image
             ) {
            String url = String.format(link,images.getUrl());
            String row = String.format(tableRow,images.getId(), images.getName(), images.getCategory(),images.getDate(),url,images.getLikes());
            builder.append(row);
        }
        return String.format(DYNAMIC_PAGE_CONTENT, builder.toString());
    }

    @Override
    public String buildTable(ImageTableData images) {
        String url = String.format(link,images.getUrl());
       String row = String.format(tableRow,images.getId(), images.getName(), images.getCategory(),images.getDate(),url,images.getLikes());
       return String.format(DYNAMIC_PAGE_CONTENT, row);
    }
}
